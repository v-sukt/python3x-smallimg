One smallest possible image for python standalone app. 
The image is based on busybox and uses static-python (https://github.com/pts/staticpython). 

Possibilities : 
 - to run small python code with builti-in libraries without any big OS requirement
 - to run python app where all requried libraries are with referece to $PWD

Docker container use:
```
docker pull vsukt/python3x-smallapp
docker run --rm vsukt/python3x-smallapp
``` 
